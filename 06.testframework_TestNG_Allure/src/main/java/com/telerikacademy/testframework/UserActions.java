package com.telerikacademy.testframework;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class UserActions {
//    String browser = Utils.getConfigPropertyByKey("browser");
    BrowserType browserType = BrowserType.valueOf(Utils.getConfigPropertyByKey("browser"));
    final WebDriver driver;
    final WebDriverWait wait;


    public UserActions() {
        this.driver = Utils.getWebDriver();
        this.wait = new WebDriverWait(driver, 30);
    }

    public static void loadBrowser() {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    //MAIN METHODS
    public void clickElement(String key) {
        Utils.LOG.info("Clicking on element " + key);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(key))));
        if (browserType == BrowserType.CHROME){
            fixedWait(500);
        }
        element.click();
    }

    public void typeValueInField(String value, String field) {
        Utils.LOG.info("Typing " + value + " in " + field);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(field))));
        element.clear();
        element.sendKeys(value);
    }

    public String getText(String locator) {
        Utils.LOG.info("Getting text of element: " + locator);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        return element.getText();
    }

    public String createRandomString(int n) {
        Utils.LOG.info("Creating random string");
        return RandomStringUtils.randomAlphabetic(n);
    }

    //Waits
    public void waitForElementVisible(String locator) {
        Utils.LOG.info("Waiting for: " + locator);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void fixedWait (int milliseconds){
        try {
            Thread.sleep(milliseconds);
        } catch (Exception nee) {
        }
    }

    //FUNCTIONAL METHODS
    //Navigate
    public void gotoHomePage(){
        Utils.LOG.info("Performing gotoHomePage");
        driver.get(Utils.getConfigPropertyByKey("base.url"));
    }

    public void goToUsersPage(){
        clickElement("user.usernameRight");
    }


    //Registration
    public void registerUser(String username, String password) {
        registerUserNegative(username, password);
        waitForElementVisible("register.confirmationHeader");
        clickElement("register.confirmationLink");
    }

    public void registerUserNegative(String username, String password) {
        Utils.LOG.info("Registering user");
        clickElement("home.signUpBtn");
        waitForElementVisible("register.headerh1");
        clickElement("register.username");
        Utils.LOG.info("Entering username: " + username);
        typeValueInField(username, "register.username");
        clickElement("register.password");
        Utils.LOG.info("Entering repeat password: " + password);
        typeValueInField(password, "register.password");
        Utils.LOG.info("Entering password: " + password);
        typeValueInField(password, "register.repeatPassword");
        clickElement("register.registerBtn");
    }

    public void registerUserNegativeMismatchingPasswords(String username, String password, String confirmPassword) {
        Utils.LOG.info("Registering user");
        clickElement("home.signUpBtn");
        waitForElementVisible("register.headerh1");
        clickElement("register.username");
        Utils.LOG.info("Entering username: " + username);
        typeValueInField(username, "register.username");
        clickElement("register.password");
        Utils.LOG.info("Entering repeat password: " + password);
        typeValueInField(password, "register.password");
        Utils.LOG.info("Entering password: " + confirmPassword);
        typeValueInField(confirmPassword, "register.repeatPassword");
        clickElement("register.registerBtn");
    }


    //LogIn & LogOut
    public void logIn(String username, String password) {
        Utils.LOG.info("Performing Login method");
        clickElement("home.loginBtn");
        typeValueInField(username, "login.username");
        typeValueInField(password, "login.password");
        clickElement("login.loginBtn");
    }

    public void loginNotRegisteredUser(){
        Utils.LOG.info("Performing Login method");
        String username = createRandomString(10);
        String password = createRandomString(10);
        logIn(username, password);
    }

    public void logOut() {
        Utils.LOG.info("Performing Logout");
        clickElement("home.logoutBtn");
    }

    //User information management
    public void updateUsersInfo(){
        clickElement("user.usernameRight");
        clickElement("user.usernameSettingsLeft");
        clickElement("user.updatePersonalInfo");
        typeValueInField(Constants.UPDATED_FIRST_NAME, "user.settings.firstName");
        typeValueInField(Constants.UPDATED_LAST_NAME, "user.settings.lastName");
        clickElement("user.settings.gender");
        clickElement("user.settings.other");
        clickElement("user.settings.privacy");
        clickElement("user.settings.private");

        //no calendar yet
        typeValueInField(Constants.UPDATED_TOWN_OF_BIRTH, "user.settings.townOfBirth");
        typeValueInField(Constants.UPDATED_COUNTRY_OF_BIRTH, "user.settings.countryOfBirth");
        typeValueInField(Constants.UPDATED_TOWN_OF_RESIDENCE, "user.settings.townOfResidence");
        typeValueInField(Constants.UPDATED_COUNTRY_OF_RESIDENCE, "user.settings.countryOfResidence");
        typeValueInField(Constants.USER_A_UPDATED_EMAIL, "user.settings.email");

        uploadUserPicture();
        clickElement("user.settings.saveChangesBtn");
    }

    public void uploadUserPicture(){
        WebElement uploadPictureButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey("user.settings.profilePhotoUploadBtn"))));
        uploadPictureButton.sendKeys(System.getProperty("user.dir") + Constants.USER_PICTURE);
    }

    //Posts
    public void createPost(String privacyLocator, String post){
        Utils.LOG.info("Creating post, " + "Privacy: " +privacyLocator);
        clickElement("post.postText");
        typeValueInField(post, "post.postText");
        clickElement(privacyLocator);
        clickElement("post.submitPostBtn");
    }

    public void uploadPictureInPost(){
        WebElement uploadPictureButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey("post.browseBtn"))));
        uploadPictureButton.sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\UploadFiles\\tree.jpg");
    }

    public void deleteAPost(){
        Utils.LOG.info("Deleting a post");
        clickElement("post.delete");
    }

    public void likePublicPost(){
        Utils.LOG.info("Liking or disliking a public post");
        clickElement("comment.likeBtnPublicPost");
    }

    //Comments
    public void createACommentPublicPost(String comment){
        Utils.LOG.info("Creating a comment: "  +comment);
        typeValueInField(comment, "comment.commentInputPublicPost");
        clickElement("comment.commentBtnPublicPost");
    }

    //Searching
    public void searchForUser(String firstnameOrEmail){
        Utils.LOG.info("Searching for user: " + firstnameOrEmail);
        clickElement("user.searchField");
        typeValueInField(firstnameOrEmail, "user.searchField");
        clickElement("user.searchIcon");
    }

    public void searchForUserAndGoToUsersPage(String firstnameOrEmail){
        Utils.LOG.info("Searching for user and redirecting to user's page: " + firstnameOrEmail);
        clickElement("user.searchField");
        typeValueInField(firstnameOrEmail, "user.searchField");
        clickElement("user.searchIcon");
        clickElement("user.pictureAfterSearch");
    }

    //Buddies
    public void usersAreBecomingBuddies(String  usernameUserB,  String  passwordUserB,String  userNameC, String  nameUserC,  String  passwordUserC){
        Utils.LOG.info("User " + usernameUserB + " user " + nameUserC +" are becoming buddies ");
        logIn(usernameUserB, passwordUserB);
        searchForUser(nameUserC);
        clickElement("user.addBuddyBtn");
        logOut();
        gotoHomePage();
        logIn(userNameC, passwordUserC);
        clickElement("user.usernameRight");
        clickElement("buddies.acceptRequest");
        clickElement("buddies.myBuddiesBtn");
    }

    //ASSERTS
    public void assertElementPresent(String locator) {
        Utils.LOG.info("Asserting presence of: " + locator);
        waitForElementVisible(locator);
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementNotPresent(String locator) {
        Utils.LOG.info("Xpath does NOT exist: " + locator);
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }

        public void assertWrongUsernameOrPasswordMessage() {
        Utils.LOG.info("Asserting wrong credentials message");
        String wrongPasswordMessage = getText("login.wrongPasswordMessage");
        Assert.assertEquals(Constants.ERROR_WRONG_USERNAME_PASSWORD, wrongPasswordMessage);
    }

    public void assertTextContains(String expectedText, String locator){
        Utils.LOG.info("Asserting " + locator + " contains text " + expectedText);
        String elementText = getText(locator);
        Assert.assertTrue(elementText.contains(expectedText));
    }

    public void assertTextEquals(String expectedText, String locator){
        WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        String textOfWebElement = webElement.getText();
        Utils.LOG.info("Expected text: " + expectedText + "; Actual text: " + textOfWebElement);
        Assert.assertEquals(expectedText, textOfWebElement);
    }
}
