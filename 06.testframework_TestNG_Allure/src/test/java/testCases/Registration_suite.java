package testCases;

import com.telerikacademy.testframework.Constants;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.*;

public class Registration_suite extends BaseTest {

    @Severity(SeverityLevel.BLOCKER)
    @Test(groups = "smoke")
    public void T01_RegisterWithValidCredentials() {
        actions.registerUser(actions.createRandomString(10), actions.createRandomString(10));
        actions.assertElementPresent("login.header");
    }

    @Severity(SeverityLevel.BLOCKER)
    @Test(groups = "smoke")
    public void T02_RegisterWithEmptyCredentials() {
        actions.registerUserNegative("", "");
        actions.assertElementPresent("register.registerBtn");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = "boundary")
    public void T03_RegisterWithValidUsernameMinSymbols() {
        actions.registerUser(actions.createRandomString(5), actions.createRandomString(10));
        actions.assertElementPresent("login.header");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = "boundary")
    public void T04_RegisterWithValidUsernameMaxSymbols() {
        actions.registerUser(actions.createRandomString(30), actions.createRandomString(10));
        actions.assertElementPresent("login.header");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = "boundary")
    public void T05_RegisterWithShorterUsername() {
        actions.registerUserNegative(actions.createRandomString(4), actions.createRandomString(10));
        actions.assertElementPresent("register.registerBtn");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = "boundary")
    public void T06_RegisterWithLongerUsername() {
        actions.registerUserNegative(actions.createRandomString(31), actions.createRandomString(10));
        actions.assertElementPresent("register.registerBtn");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = "boundary")
    public void T07_RegisterWithValidPasswordMinSymbols() {
        actions.registerUser(actions.createRandomString(10), actions.createRandomString(5));
        actions.assertElementPresent("login.header");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = "boundary")
    public void T08_RegisterWithValidPasswordMaxSymbols() {
        actions.registerUser(actions.createRandomString(10), actions.createRandomString(25));
        actions.assertElementPresent("login.header");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = "boundary")
    public void T09_RegisterWithShorterPassword() {
        actions.registerUserNegative(actions.createRandomString(10), actions.createRandomString(4));
        actions.assertElementPresent("register.registerBtn");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = "boundary")
    public void T10_RegisterWithLongerPassword() {
        actions.registerUserNegative(actions.createRandomString(10), actions.createRandomString(26));
        actions.assertElementPresent("register.registerBtn");
    }

    @Severity(SeverityLevel.BLOCKER)
    @Test
    public void T11_RegisterWithMismatchingPasswords() {
        actions.registerUserNegativeMismatchingPasswords(actions.createRandomString(10), actions.createRandomString(5), actions.createRandomString(6));
        actions.assertElementPresent("register.registerBtn");
    }

    //expected to fail - special symbols in username are not considered as a good practice
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void T12_RegisterWithSpecialSymbolInUsername() {
        actions.registerUserNegative("@" + actions.createRandomString(10), actions.createRandomString(10));
        actions.assertElementNotPresent("register.confirmationHeader");
    }

    @Severity(SeverityLevel.BLOCKER)
    @Test(groups = "smoke")
    public void T13_RegisterAlreadyRegisteredUser() {
        actions.registerUserNegative(Constants.USER_A_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.assertElementPresent("register.errorMessageUserExists");
    }
}


