package testCases;

import com.telerikacademy.testframework.Constants;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

public class Login_suite extends BaseTest {

    @Severity(SeverityLevel.BLOCKER)
    @Test
    public void Т01_LogInWithValidCredentials() {
        actions.logIn(Constants.USER_A_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.assertElementPresent("user.usernameRight");
        actions.logOut();
    }

    @Severity(SeverityLevel.BLOCKER)
    @Test
    public void Т02_LogInWithEmptyCredentials() {
        actions.logIn("", "");
        actions.assertWrongUsernameOrPasswordMessage();
    }

    @Severity(SeverityLevel.BLOCKER)
    @Test
    public void Т03_LogInNotRegisteredUser() {
        actions.loginNotRegisteredUser();
        actions.assertWrongUsernameOrPasswordMessage();
    }

    @Severity(SeverityLevel.BLOCKER)
    @Test(groups = "smoke")
    public void Т04_LogOut() throws InterruptedException {
        actions.logIn(Constants.USER_A_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.logOut();
        actions.assertElementPresent("login.header");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test
    public void Т05_LogInWithExistingUserLowerCaseUsername() throws InterruptedException {
        actions.logIn(Constants.USER_A_USERNAME.toLowerCase(), Constants.ALL_USERS_VALID_PASSWORD);
        actions.assertElementPresent("user.usernameRight");
        actions.fixedWait(1000);
        actions.logOut();
    }

    @Severity(SeverityLevel.BLOCKER)
    @Test
    public void Т06_LogInWithExistingUserLowerCasePassword() {
        actions.logIn(Constants.USER_A_USERNAME, Constants.ALL_USERS_VALID_PASSWORD.toLowerCase());
        actions.assertElementPresent("login.wrongPasswordMessage");
    }
}
