package socialnetwork.exceptions;

public class RequestAlreadySentException extends Exception {
    public RequestAlreadySentException() {super("You have already sent this user a request!");}
}
