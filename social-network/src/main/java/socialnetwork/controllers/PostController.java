package socialnetwork.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import socialnetwork.models.Comment;
import socialnetwork.models.Post;
import socialnetwork.models.User;
import socialnetwork.services.contracts.CommentService;
import socialnetwork.services.contracts.PostService;
import socialnetwork.services.contracts.UserService;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping
public class PostController {

    private UserService userService;
    private PostService postService;
    private CommentService commentService;

    @Autowired
    public PostController(UserService userService, PostService postService, CommentService commentService) {
        this.userService = userService;
        this.postService = postService;
        this.commentService = commentService;
    }

    @PostMapping("/users/{userName}")
    public String submitPost(@PathVariable String userName, @Valid @ModelAttribute("newPost") Post post, BindingResult bindingResult, Principal principal, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Please enter some text");
            return "redirect:/users/" + userName;
        }

        postService.createPost(post, principal.getName());
        return "redirect:/users/" + userName;
    }

    @GetMapping("/users/{postID}/like/{userName}")
    public String likePost(@PathVariable int postID, @PathVariable String userName, Principal principal) {
        Post post = postService.findPostByID(postID);
        postService.likePost(principal.getName(), post);
        return "redirect:/users/" + userName;
    }

    @GetMapping("/users/{postID}/likeFeedPost/{userName}")
    public String likeFeedPost(@PathVariable int postID, @PathVariable String userName, Principal principal) {
        Post post = postService.findPostByID(postID);
        postService.likePost(principal.getName(), post);
        return "redirect:/";
    }

    @GetMapping("/posts/{postID}/likes")
    public int postLikes(@PathVariable int postID ) {
        return postService.getNumberOfLikes(postID);
    }

    @PostMapping("/users/comment/{userName}/{postID}")
    public String commentOnPost(@PathVariable String userName,@Valid @ModelAttribute("newComment") Comment comment,
                                BindingResult bindingResult, @PathVariable int postID, Principal principal, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "You didn't enter any text for the comment");
            return "redirect:/users/" + userName;
        }
        commentService.createComment(principal.getName(),postID,comment);
        return "redirect:/users/" + userName;
    }

    @GetMapping("/posts/delete/{postID}/{userName}")
    public String deletePost(@PathVariable String userName, @PathVariable int postID,
                             Principal principal){
        User user = postService.findPostByID(postID).getUser();
        if (!user.getUsername().equals(principal.getName())) {
            throw new IllegalStateException();
        }
        try {
            postService.removePost(postID);
        }
        catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        return "redirect:/users/" + userName;
    }

    @GetMapping("/users/comment/{userName}/{postID}")
    public String commentOnPost(@PathVariable String userName, @PathVariable int postID, Model model) {
        model.addAttribute("newComment", new Comment());
        return "redirect:/users/" + userName;
    }
}