package socialnetwork.services.contracts;

import org.springframework.web.multipart.MultipartFile;
import socialnetwork.exceptions.PostMismatchException;
import socialnetwork.models.Comment;
import socialnetwork.models.Post;
import socialnetwork.models.User;

import java.io.IOException;
import java.util.List;

public interface PostService {

    List<Post> findAll();

    Post createPost(Post post, String userName);

    Post updatePost(Post post, String text, MultipartFile picture) throws PostMismatchException, IOException;

    void removePost(int id);

    Post findPostByID(int postID);

    List<Post> getUserPosts(User user);

    List<Comment> getPostComments(Post post);

    void likePost(String userName, Post post);

    int getNumberOfLikes(int postID);

    List<Post> getPublicFeed();

    List<Post> getUserFeed(User user);
}
