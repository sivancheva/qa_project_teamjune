package socialnetwork.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import socialnetwork.exceptions.DuplicateUsernameException;
import socialnetwork.models.User;
import socialnetwork.repositories.UserRepository;
import socialnetwork.services.contracts.UserService;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findAllActivated() {
        return userRepository.findAllActivated();
    }

    @Override
    public User createUser(User user) throws DuplicateUsernameException {
        boolean usernameExists = userRepository.findAll().stream()
                .anyMatch(u -> u.getUsername().equals(user.getUsername()));
        if (usernameExists){
            throw new DuplicateUsernameException();
        }
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(String userName) {
        User userToBeDeleted = findByUserName(userName);

        if(!userToBeDeleted.isActivated()) {
            throw new IllegalStateException(String.format("User %s has already deactivated his account!", userName));
        }
        else {
            userToBeDeleted.setActivated(false);
        }
    }

    @Transactional
    @Override
    public User updateUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User changeProfilePic(User user, MultipartFile photo) throws IOException {
        user.setProfilePhoto(photo);
        return userRepository.save(user);
    }

    @Override
    public Set<User> userSearch(String search) {
        String[] splitSearch = search.split(" ");

        if (splitSearch.length == 1) {
            if (splitSearch[0].contains("@")) {
                return userRepository.searchByEmail(splitSearch[0]);
            }
            return userRepository.searchByOneName(splitSearch[0]);
        }
        else {
            return userRepository.searchByTwoNames(splitSearch[0], splitSearch[1]);
        }
    }

    @Override
    public void addBuddy(String receiverUsername, String senderUsername) {
        User sender = findByUserName(senderUsername);
        User receiver = findByUserName(receiverUsername);
        sender.getFriends().add(receiver);
        receiver.getFriends().add(sender);
        receiver.getRequestsReceived().remove(sender);
        userRepository.save(sender);
    }

    @Override
    public void removeBuddy(String senderUsername, String receiverUsername) {
        User sender = findByUserName(senderUsername);
        User receiver = findByUserName(receiverUsername);
        sender.getFriends().remove(receiver);
        receiver.getFriends().remove(sender);
        userRepository.save(sender);
    }

    @Override
    public void denyRequest(String senderUsername, String receiverUsername) {
        User sender = findByUserName(senderUsername);
        User receiver = findByUserName(receiverUsername);
        receiver.getRequestsReceived().remove(sender);
        userRepository.save(sender);
    }

    @Override
    public void sendRequest(String receiverUsername, String senderUsername) {
        User sender = findByUserName(senderUsername);
        User receiver = findByUserName(receiverUsername);
        receiver.getRequestsReceived().add(sender);
        userRepository.save(receiver);
    }

    @Override
    public void cancelRequest(String receiverUsername, String senderUsername) {
        User sender = findByUserName(senderUsername);
        User receiver = findByUserName(receiverUsername);
        sender.getRequestsSent().remove(receiver);
        userRepository.save(sender);
    }

}