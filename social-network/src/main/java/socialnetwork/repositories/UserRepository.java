package socialnetwork.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import socialnetwork.models.User;

import java.util.List;
import java.util.Set;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    @Query("select u from User u where u.username = :username and u.activated = true")
    User findByUserName(@Param("username") String username);

    @Query("select u from User u where u.email = :email and u.activated = true")
    Set<User> searchByEmail(@Param("email") String email);

    @Query("select u from User u where u.firstName = :name and u.activated = true " +
            "or u.lastName = :name and u.activated = true")
    Set<User> searchByOneName(@Param("name") String name);

    @Query("select u from User u where u.firstName = :firstName and u.lastName = :secondName and u.activated = true " +
            "or u.firstName = :secondName and u.lastName = :firstName and u.activated = true")
    Set<User> searchByTwoNames(@Param("firstName") String firstName, @Param("secondName") String secondName);

    List<User> findAll();

    @Query("select u from User u where u.activated = true")
    List<User> findAllActivated();

}