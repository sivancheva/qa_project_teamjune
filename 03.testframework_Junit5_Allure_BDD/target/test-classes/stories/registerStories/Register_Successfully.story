Meta:
@smoke

Narrative:
As a not registered user
I want to register in Social Network Application
So that I can log in

Scenario: New user register successfully
Given I am on a home page
When I register with randomUser3 and randomPassword
Then I assert presence on login page
And I am on a home page
When As registered user I am logging in with randomUser3 and randomPassword
Then I assert presence on user's page