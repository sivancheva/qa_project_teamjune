package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions {
    BrowserType browserType = BrowserType.valueOf(Utils.getConfigPropertyByKey("browser"));
    final WebDriver driver;
    final WebDriverWait wait;

    public UserActions() {
        this.driver = Utils.getWebDriver();
        this.wait = new WebDriverWait(driver, 30);
    }

    public static void loadBrowser() {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    //MAIN METHODS
    public void clickElement(String key) {
        Utils.LOG.info("Clicking on element " + key);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(key))));
        if (browserType == BrowserType.CHROME){
            fixedWait(500);
        }
        element.click();
    }

    public void typeValueInField(String value, String field) {
        Utils.LOG.info("Typing " + value + " in " + field);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(field))));
        element.clear();
        element.sendKeys(value);
    }

    public String getText(String locator) {
        Utils.LOG.info("Getting text of element: " + locator);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        return element.getText();
    }

    public void fixedWait(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (Exception nee) {
        }
    }

    //Waits
    public void waitForElementVisible(String locator) {
        Utils.LOG.info("Waiting for: " + locator);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    //FUNCTIONAL METHODS
    //User information management
    public void updateUsersInfo() {

        clickElement("user.usernameRight");
        clickElement("user.usernameSettingsLeft");
        clickElement("user.updatePersonalInfo");
        typeValueInField(Consts.UPDATED_FIRST_NAME, "user.settings.firstName");
        typeValueInField(Consts.UPDATED_LAST_NAME, "user.settings.lastName");
        clickElement("user.settings.gender");
        clickElement("user.settings.other");
        clickElement("user.settings.privacy");
        clickElement("user.settings.private");

        //no calendar yet
        typeValueInField(Consts.UPDATED_TOWN_OF_BIRTH, "user.settings.townOfBirth");
        typeValueInField(Consts.UPDATED_COUNTRY_OF_BIRTH, "user.settings.countryOfBirth");
        typeValueInField(Consts.UPDATED_TOWN_OF_RESIDENCE, "user.settings.townOfResidence");
        typeValueInField(Consts.UPDATED_COUNTRY_OF_RESIDENCE, "user.settings.countryOfResidence");
        typeValueInField(Consts.USER_A_UPDATED_EMAIL, "user.settings.email");

        clickElement("user.settings.saveChangesBtn");
    }

    //LogIn & LogOut
    public void logIn(String username, String password) {
        Utils.LOG.info("Performing Login method");
        clickElement("home.loginBtn");
        typeValueInField(username, "login.username");
        typeValueInField(password, "login.password");
        clickElement("login.loginBtn");
    }

    public void logOut() {
        Utils.LOG.info("Performing Logout");
        clickElement("home.logoutBtn");
    }

    //Registration
    public void registerUser(String username, String password) {
        registerUserNegative(username, password);
        waitForElementVisible("register.confirmationHeader");
        clickElement("register.confirmationLink");
    }

    public void registerUserNegative(String username, String password) {
        Utils.LOG.info("Registering user");
        clickElement("home.signUpBtn");
        waitForElementVisible("register.headerh1");
        clickElement("register.username");
        Utils.LOG.info("Entering username: " + username);
        typeValueInField(username, "register.username");
        clickElement("register.password");
        Utils.LOG.info("Entering repeat password: " + password);
        typeValueInField(password, "register.password");
        Utils.LOG.info("Entering password: " + password);
        typeValueInField(password, "register.repeatPassword");
        clickElement("register.registerBtn");
    }

    //Navigate
    public void gotoHomePage() {
        Utils.LOG.info("Performing gotoHomePage");
        driver.get(Utils.getConfigPropertyByKey("base.url"));
    }

    //Searching
    public void searchForUser(String firstnameOrEmail) {
        Utils.LOG.info("Searching for user: " + firstnameOrEmail);
        clickElement("user.searchField");
        typeValueInField(firstnameOrEmail, "user.searchField");
        clickElement("user.searchIcon");
    }

    //Buddies
    public void sendBuddyRequest(String nameOrEmail) {
        Utils.LOG.info("Sending buddy request to " + nameOrEmail);
        searchForUser(nameOrEmail);
        clickElement("user.addBuddyBtn");
        logOut();
        gotoHomePage();
    }

    public void acceptBuddyRequest() {
        Utils.LOG.info("Accepting buddy request");
        clickElement("user.usernameRight");
        clickElement("buddies.acceptRequest");
    }

    public void removeBuddy(String nameOrEmail) {
        Utils.LOG.info("Removing buddy");
        searchForUser(nameOrEmail);
        clickElement("user.removeBuddyBtn");
    }

    //ASSERTS
    //MAIN ASSERTS
    public void assertElementPresent(String locator) {
        Utils.LOG.info("Asserting presence of: " + locator);
        waitForElementVisible(locator);
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementNotPresent(String locator) {
        Utils.LOG.info("Xpath does NOT exist: " + locator);
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }

    public void assertTextContains(String expectedText, String locator) {
        Utils.LOG.info("Asserting " + locator + " contains text " + expectedText);
        String elementText = getText(locator);
        Assert.assertTrue(elementText.contains(expectedText));
    }

    //FUNCTIONAL ASSERTS
    //Buddies
    public void assertBuddyInList(String locator) {
        clickElement("buddies.myBuddiesBtn");
        assertElementPresent(locator);
    }

    public void assertBuddyNotInList(String locator) {
        clickElement("user.usernameRight");
        clickElement("buddies.myBuddiesBtn");
        assertElementNotPresent(locator);
        logOut();
        gotoHomePage();
    }

    //Posts
    public void assertPostIsVisibleInPersonalizedFeed(String locator) {
        Utils.LOG.info("Asserting post is visible");
        assertElementPresent(locator);
    }

    public void assertPostIsNotInPersonalizedFeed(String locator) {
        Utils.LOG.info("Asserting post is not visible");
        assertElementNotPresent(locator);
    }
}
