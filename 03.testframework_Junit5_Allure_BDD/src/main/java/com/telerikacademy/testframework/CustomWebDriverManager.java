package com.telerikacademy.testframework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;


class CustomWebDriverManager {
    public enum CustomWebDriverManagerEnum {
        INSTANCE;
        String browser = Utils.getConfigPropertyByKey("browser");

        private WebDriver driver = setupBrowser(browser);

        private WebDriver setupBrowser(String browser) {
            WebDriver driver = null;

            switch (browser) {
                case "FIREFOX":
                    WebDriverManager.firefoxdriver().setup();
                    FirefoxProfile firefoxProfile = new FirefoxProfile();
                    firefoxProfile.setPreference("browser.privatebrowsing.autostart", true);
                    WebDriver firefoxDriver = new FirefoxDriver();
                    firefoxDriver.manage().window().maximize();
                    driver = firefoxDriver;
                    break;
                case "CHROME":
                    WebDriverManager.chromedriver().setup();
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("incognito");
                    WebDriver chromeDriver = new ChromeDriver(options);
                    chromeDriver.manage().window().maximize();
                    driver = chromeDriver;
                    break;
            }
            return driver;
        }

        public void quitDriver() {
            if (driver != null) {
                driver.quit();
            }
        }

        public WebDriver getDriver() {
            return driver;
        }
    }
}
