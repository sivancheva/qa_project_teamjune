package stepdefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.*;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();

    @AfterScenario
    public void LogOut(){
        actions.logOut();
        actions.gotoHomePage();
    }

    //go to homepage
    @Given("I am on a home page")
    @When("I am on a home page")
    @Then("I am on a home page")
    public void goToHomePage(){
        actions.gotoHomePage();
    }

    //log in
    @Given("As registered user I am logging in with $username and $password")
    @When("As registered user I am logging in with $username and $password")
    @Then("As registered user I am logging in with $username and $password")
    @Alias("I log in $username and $password")
    public void logIn(String username, String password){
        actions.logIn(username, password);
    }

    //registration
    @When("I register with $username and $password")
    public void registerUser(String username, String password){

        actions.registerUser(username, password);
    }

    //update personal information
    @When("I update my personal Information")
    public void updateUsersInfo(){
        actions.updateUsersInfo();
    }

    //log out
    @When("I log out")
    public void logOut(){
        actions.logOut();
    }


    //buddies
    @When("I send buddy request to $name")
    public void sendBuddyRequest (String nameOrEmail){
        actions.sendBuddyRequest(nameOrEmail);
    }

    @When("Buddy request is accepted")
    public void acceptBuddyRequest(){
        actions.acceptBuddyRequest();
    }

    @When("I remove $name from my buddies")
    public void removeBuddy(String nameOrEmail){
        actions.removeBuddy(nameOrEmail);
    }

    //Assertions
    @Then ("I assert $user is in my buddy list")
    public void assertBuddyInList(String locator){
        actions.assertBuddyInList(locator);
    }

    @Then("I assert $user is not in my buddy list")
    public void assertBuddyNotInList(String locator){
        actions.assertBuddyNotInList(locator);
    }

    @Then("I assert $post is visible in feed")
    public void assertPostIsVisibleInPersonalizedFeed(String locator){
        actions.assertPostIsVisibleInPersonalizedFeed(locator);
    }

    @Then("I assert $post is not visible in feed")
    public void assertPostIsNotInPersonalizedFeed(String locator){
        actions.assertPostIsNotInPersonalizedFeed(locator);
    }

    @Then("I assert presence on home page")
    public void assertPresenceOnHomepage(){
        actions.assertElementPresent("home.signUpBtn");
    }

    @Then("I assert presence on user's page")
    public void assertPresenceOnUserPage(){
        actions.assertElementPresent("user.usernameRight");
    }

    @Then("I assert presence on login page")
    public void assertPresenceOnLoginPage(){
        actions.assertElementPresent("login.header");
    }

    @Then("I assert existence of $text in $element")
    public void assertTextEquals(String name, String locator){
        actions.assertTextContains(name, locator);
    }






}
