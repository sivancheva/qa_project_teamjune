# QA Project Social Network TeamJune

## 1. Tasks tracking - [Trello Board](https://trello.com/b/5nvFIXXW/qafinalprojectteamjune)

## 2. Test plan - [Test Plan Social Network Team June](https://drive.google.com/file/d/1RoZ5PYyUDjSc6fo9PZkpeBSncDrUejpn/view?usp=sharing)

## 3. Test cases - Test cases are written in TestRail. Access gained to:

*  Pavlina Koleva -Telerik Academy
*  Vladimir Venkov - Telerik Academy
*  Petya Petarcheva - ScaleFocus
 
## 4. Test cases - [Test cases Google Drive](https://docs.google.com/spreadsheets/d/1NQe-bo_X6LsmYTeskCO3l0o17-rbvwiP36S-LbDwADA/edit?usp=sharing)

## 5. Issues list - [Issues list](https://gitlab.com/Stan23/social-network/issues)

## 6. Test report - [Test Report](https://drive.google.com/file/d/1fPuYV0vkcTiEw0dCftoNJzhLZFYFXkWb/view?usp=sharing)

## 7. Issues report - [Issues Report](https://drive.google.com/file/d/1E0T0Q5yVarzbCpIGa-QXuwjVjDtQKlCC/view?usp=sharing)

## 8. Report Exploratory Testing - [Report Exploratory Testing](https://drive.google.com/file/d/1Fsw-_0_OLLYohCR8uJrZ7Z1Y82lwub7V/view?usp=sharing)

